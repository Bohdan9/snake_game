package com.epam.game;

import com.epam.models.Constant;
import com.epam.models.Snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SnakeGame extends JPanel implements ActionListener {
    private Constant constant = new Constant();
    private int speed = constant.TEN;
    private Graphics globalGraphics;
    private Snake s = new Snake(constant.TEN, constant.TEN, constant.NINE, constant.TEN);
    private Timer timer = new Timer(constant.THOUSAND / speed, this);
    private JFrame jFrame = new JFrame("Snake");
    public SnakeGame() {
        timer.start();
        addKeyListener(new KeyBoard());
        setFocusable(true);
    }

    public void paint(final Graphics graphics) {
        globalGraphics = graphics.create();
    }

    private void drawGame(final Graphics graphics) {
        paintGrid(graphics);
        paintApple(graphics);
        paintPoisonApple(graphics);
        paintSnake(graphics);
        paintScore(graphics);
    }

    private void paintGrid(final Graphics graphics) {
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, constant.WIDTH * constant.SCALE, constant.HEIGHT * constant.SCALE);
        for (int x = 0; x <= constant.WIDTH * constant.SCALE; x += constant.SCALE) {
            graphics.setColor(Color.black);
            graphics.drawLine(x, 0, x, constant.HEIGHT * constant.SCALE);
        }
        for (int y = 0; y <= constant.HEIGHT * constant.SCALE; y += constant.SCALE) {
            graphics.setColor(Color.black);
            graphics.drawLine(0, y, constant.WIDTH * constant.SCALE, y);
        }
    }

    private void paintPoisonApple(final Graphics graphics) {
        if (s.getLength() > constant.TEN) {
            graphics.setColor(Color.red);
            graphics.fillOval(s.getPoisonApple().getPositionX() * constant.SCALE + 2,
                    s.getPoisonApple().getPositionY() * constant.SCALE + 6,
                    constant.SCALE - 10, constant.SCALE - 10);
            graphics.drawString("Red apple is a poison apple. Be careful! ", 0,
                    constant.HEIGHT * constant.SCALE - 80);
        }
    }

    private void paintApple(final Graphics graphics) {
        graphics.setColor(Color.green);
        graphics.fillOval(s.getApple().getPositionX() * constant.SCALE + 4, s.getApple().getPositionY() * constant.SCALE + 4,
                constant.SCALE - 10, constant.SCALE - 10);
    }

    private void paintSnake(final Graphics graphics) {
        for (int d = 1; d < s.getLength(); d++) {
            graphics.setColor(Color.CYAN);
            graphics.fillRect(s.getSnakeX()[d] * constant.SCALE + 1, s.getSnakeY()[d] * constant.SCALE + 1,
                    constant.SCALE - 6, constant.SCALE - 6);
            graphics.setColor(Color.PINK);
            graphics.fillRect(s.getSnakeX()[0] * constant.SCALE + 1, s.getSnakeY()[0] * constant.SCALE + 1,
                    constant.SCALE - 6, constant.SCALE - 6);
        }
        graphics.setColor(Color.white);
    }

    private void paintScore(final Graphics graphics) {
        graphics.drawString("Score: " + s.getScore(), 0, constant.HEIGHT * constant.SCALE - 10);
        graphics.drawString("Best score: " + s.getBestScore(), 0, constant.HEIGHT * constant.SCALE - 30);
    }

    public void createWindow() {
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setResizable(false);
        jFrame.setSize(constant.WIDTH * constant.SCALE + 10, constant.HEIGHT * constant.SCALE + 20);
        jFrame.setLocationRelativeTo(null);
        jFrame.add(new SnakeGame());
        jFrame.setVisible(true);
        jFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        try {
            drawGame(globalGraphics);
            s.move();
            s.transitionFunc();
            s.eatApple();
            timer.stop();
            s.eatPoisonApple();
            timer.start();
            s.getBestsScore();
            timer.stop();
            s.eatMyself();
            timer.start();
            repaint();
        }catch (NullPointerException ex){

        }
    }
    public class KeyBoard extends KeyAdapter {
        public void keyPressed(final KeyEvent event) {
            switch (event.getKeyCode()) {
                case KeyEvent.VK_UP:
                    if (s.getDirection() != constant.ONE) {
                        s.setDirection(constant.THREE);
                    }
                    break;
                case KeyEvent.VK_DOWN:
                    if (s.getDirection() != constant.THREE) {
                        s.setDirection(constant.ONE);
                    }
                    break;
                case KeyEvent.VK_RIGHT:
                    if (s.getDirection() != constant.TWO) {
                        s.setDirection(constant.NULL);
                    }
                    break;
                case KeyEvent.VK_LEFT:
                    if (s.getDirection() != constant.NULL) {
                        s.setDirection(constant.TWO);
                    }
                    break;
                default:
                    System.out.println("-");
            }
        }
    }
}


package com.epam;

import com.epam.game.SnakeGame;

public class Main {

    public static void main(String[] args) {
        SnakeGame snakeGameView = new SnakeGame();
        snakeGameView.createWindow();
    }
}

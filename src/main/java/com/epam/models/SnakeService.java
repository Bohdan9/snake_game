package com.epam.models;

public interface SnakeService {

    void eatApple();

    void eatPoisonApple();

    void eatMyself();

    void move();

    void transitionFunc();
}

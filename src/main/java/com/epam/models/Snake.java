package com.epam.models;

import javax.swing.*;


public class Snake implements SnakeService {
    private Constant constant = new Constant();
    private Apple apple = new Apple(Math.abs((int) (Math.random() * constant.WIDTH - 1)),
            Math.abs((int) (Math.random() * constant.HEIGHT - 1)));
    private PoisonApple poisonApple = new PoisonApple(Math.abs((int) (Math.random() * constant.WIDTH - 5)),
            Math.abs((int) (Math.random() * constant.HEIGHT - 3)));

    private int length = 2;
    private int direction = 0;
    private int score;
    private int bestScore;
    private int snakeX[] = new int[constant.WIDTH * constant.SCALE];
    private int snakeY[] = new int[constant.HEIGHT * constant.SCALE];

    public Snake() {

    }

    public Snake(final int x0, final int y0, final int x1, final int y1) {
        snakeX[0] = x0;
        snakeY[0] = y0;
        snakeX[1] = x1;
        snakeY[1] = y1;
    }

    public int getBestScore() {
        return bestScore;
    }

    public void setBestScore(final int bestScore) {
        this.bestScore = bestScore;
    }

    public Apple getApple() {
        return apple;
    }

    public void setApple(Apple apple) {
        this.apple = apple;
    }

    public PoisonApple getPoisonApple() {
        return poisonApple;
    }

    public void setPoisonApple(PoisonApple poisonApple) {
        this.poisonApple = poisonApple;
    }

    public int getLength() {
        return length;
    }

    public void setLength(final int length) {
        this.length = length;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(final int direction) {
        this.direction = direction;
    }

    public int getScore() {
        return score;
    }

    public void setScore(final int score) {
        this.score = score;
    }

    public int[] getSnakeX() {
        return snakeX;
    }

    public void setSnakeX(final int[] snakeX) {
        this.snakeX = snakeX;
    }

    public int[] getSnakeY() {
        return snakeY;
    }

    public void setSnakeY(final int[] snakeY) {
        this.snakeY = snakeY;
    }

    @Override
    public void move() {
        for (int l = length; l > 0; l--) {
            snakeX[l] = snakeX[l - 1];
            snakeY[l] = snakeY[l - 1];
        }
        switch (direction) {
            case 0:
                snakeX[0]++;
                break;
            case 1:
                snakeY[0]++;
                break;
            case 2:
                snakeX[0]--;
                break;
            case 3:
                snakeY[0]--;
                break;
            default:
                System.out.println("-");
        }
    }

    @Override
    public void transitionFunc() {
        if (snakeX[0] > constant.WIDTH - 1) {
            snakeX[0] = 0;
        }
        if (snakeX[0] < 0) {
            snakeX[0] = constant.WIDTH - 1;
        }
        if (snakeY[0] > constant.HEIGHT - 1) {
            snakeY[0] = 0;
        }
        if (snakeY[0] < 0) {
            snakeY[0] = constant.HEIGHT - 1;
        }
    }

    public void getBestsScore() {
        if (score > bestScore) {
            bestScore = score;
        }
    }

    @Override
    public void eatApple() {
        if ((snakeX[0] == apple.getPositionX()) && (snakeY[0] == apple.getPositionY())) {
            poisonApple.setRandomPosition();
            apple.setRandomPosition();
            score += constant.TEN;
            length++;
        }
    }

    @Override
    public void eatPoisonApple() {
        if ((snakeX[0] == poisonApple.getPositionX()) && (snakeY[0] == poisonApple.getPositionY())
                && (length > constant.TEN)) {
            JOptionPane.showMessageDialog(null, constant.GAMEOVERPOISON + score);
            poisonApple.setRandomPosition();
            set();
        }
    }

    @Override
    public void eatMyself() {
        for (int l = 1; l < length; l++) {
            if ((snakeX[l] == apple.getPositionX()) && (snakeY[l] == apple.getPositionY())) {
                apple.setRandomPosition();
                poisonApple.setRandomPosition();
            }
            if ((snakeX[0] == snakeX[l]) && (snakeY[0] == snakeY[l])) {
                JOptionPane.showMessageDialog(null, constant.GAME_OVER + score);
                set();
            }
        }
    }

    private void set() {
        length = 2;
        score = 0;
        direction = 0;
        apple.setRandomPosition();
    }
}


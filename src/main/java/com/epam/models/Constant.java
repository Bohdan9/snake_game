package com.epam.models;

public class Constant {
    public  final int SCALE = 32;
    public  final int WIDTH = 20;
    public  final int HEIGHT = 20;
    public final String GAME_OVER = "Game Over!" + " "
            + "You eat yourself."
            + "Press OK to try again"
            + "Your score is:";
    public final String GAMEOVERPOISON = "Game Over! + " + " "
            + "You just eat poison apple and die!!!"
            + "Press OK to try again"
            + "Your score is :";
    public final int NULL = 0;
    public final int ONE = 1;
    public final int TWO = 2;
    public final int THREE = 3;
    public final int NINE = 9;
    public final int TEN = 10;
    public final int THOUSAND = 1000;
}